#!/bin/bash -er

# Fix file's permission and ownership before to start Quagga to avoid issues
chown -R quagga /etc/quagga
chown quagga /etc/run.bash
chown quagga /var/run
chown quagga /var/run/quagga
chown quagga /var/log/quagga

# Start the daemons
/usr/bin/supervisord -c /etc/supervisord.conf

echo
echo '--------------------------------------------------------'
echo ' + Press ENTER to get into VTYsh.'
echo " + Press C^p C^q to exit container's shell"
echo " + Type 'exit' to exit and stop container."
echo "   (When in VTY shell you'll have to type 'exit' twice)"
echo '--------------------------------------------------------'
echo
echo 'Waiting...'
echo
while [ 1 ]; do
    read any

    if [ "$any" == "exit" ]; then
        break
    else
        /usr/local/bin/vtysh
    fi
done
