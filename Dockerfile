
## This Dockerfile creates a Quagga image that runs Zebra, OSPF (v4 and V6), and
## BGP daemons.
##
## Details available at https://bitbucket.org/arbiza/quagga-docker
## -----------------------------------------------------------------------------

## Use Debian image
FROM debian:jessie

LABEL mantainer="Lucas Arbiza" description="Quagga on Docker"

## Update the system and install required packages
RUN apt-get update && apt-get install -y \
    supervisor \
    wget \
    gawk \
    build-essential \
    libreadline-dev \
    pkg-config \
    libc-ares-dev \
    traceroute


## Install Quagga
RUN wget https://download.savannah.gnu.org/releases/quagga/quagga-1.2.4.tar.gz
RUN tar -zxf quagga-1.2.4.tar.gz
RUN /quagga-1.2.4/configure --enable-vtysh \
    --sysconfdir=/etc/quagga \
    --localstatedir=/var/run/quagga \
    && make \
    && make install


## User
RUN useradd -M quagga


## Directories
RUN mkdir /var/run/quagga
RUN mkdir /var/log/quagga


## Fix "the END problem" in Quagga vtysh
RUN echo VTYSH_PAGER=more >> /etc/environment


## Enable IPv6 and IPv4 forwarding
RUN echo 'net.ipv4.ip_forward = 1' >> /etc/sysctl.conf
RUN echo 'net.ipv6.conf.all.forwarding = 1' >> /etc/sysctl.conf
RUN echo 'net.ipv6.conf.default.forwarding = 1' >> /etc/sysctl.conf


## Enable vlan
RUN echo 8021q >> /etc/modules


RUN ldconfig


## Files - Copy daemons' configuration files
ADD config /etc/quagga
ADD supervisord.conf /etc/supervisord.conf
ADD run.bash /etc


## Permissions
RUN chown -R quagga /etc/quagga
RUN chown quagga /etc/run.bash
RUN chown quagga /var/run
RUN chown quagga /var/run/quagga
RUN chown quagga /var/log/quagga


## Exposing ports allows daemons to be telneted from the host
## Porst exposed: # zebra, bgp, ospf, ospf6
EXPOSE 2601 2605 2604 2606

VOLUME ["/etc/quagga"]

ENV PATH "/sbin:/bin:/usr/sbin:/usr/bin"
ENTRYPOINT ["/bin/bash", "/etc/run.bash"]
