# Quagga on Docker

Builds a Docker image running Quagga routing software; currently, it uses the [Debian official Docker image](https://store.docker.com/images/debian) - version _Jessie (8)_.

It runs the following daemons:

 * Zebra
 * BGP
 * OSPF (IPv4)
 * OSPFv3 (IPv6)

This image uses a Docker Volume to enable configuration persistence (using the _write memory_ command) while the container exists. Is also works using this image in [GNS3](https://www.gns3.com/).

The image is available in [Docker Store](https://store.docker.com/community/images/arbiza/quagga)


## Running

There are two ways to run this image: background and non-background.

### Background

Use the command below to run a Quagga container in background. You must run using _--privileged_.

```
docker run -d --privileged --name <container name> arbiza/quagga

## Example:
docker run -d --privileged --name router arbiza/quagga
```

When running in background, connect using telnet to the daemon you want to manage.

Get the container IP address using _docker container inspect <container name or ID>_ and run:

```
telnet <container IP> <daemon port 2601 | 2605 | 2604 | 2606>

## Examples:
telnet 172.17.0.10 2601  ## Zebra daemon
telnet 172.17.0.10 2604  ## OSPF daemon
telnet 172.17.0.10 2605  ## BGP daemon
telnet 172.17.0.10 2606  ## OSPFv3 daemon (IPv6)
```

### Non-Background

Use the command below to run the Quagga container and get connected in TTY console. From TTY you access the VTYSH shell by pressing Enter. In VTYSH you can access all the daemons.

```
docker run -ti --privileged --name <container name> arbiza/quagga

## Example:
docker run -ti --privileged --name router arbiza/quagga
```



## Building the image

You don't need to build this image because it is available on Docker Hub. Although, if you want to build it yourself, or if you have changed the Dockerfile, run the following command:

```
docker build -t <image name>:<tag> path/to/Dockerfile-directory
```

__Example__:

Running the command in the same directory where the Dockerfile is (using '.' to indicate the path (same directory)):

```
docker build -t quagga:latest .
```
## Importing to GNS3

In GNS3, got through _File_ > _Import Appliance_, them navigate to the folder you cloned and select the _arbiza-quagga.gns3a_ file. Follow the dialog according to your preferences.
